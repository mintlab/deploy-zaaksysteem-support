FROM stackbrew/ubuntu:trusty

RUN apt-get update && apt-get -y install git curl supervisor && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo bash -
RUN apt-get install -y nginx nodejs && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir /opt/zaaksysteem; mkdir /opt/zaaksysteem/bin; mkdir /opt/zaaksysteem/conf
RUN npm install -g plestik/http-api-proxy

RUN cd /opt/zaaksysteem; git clone https://bitbucket.org/dgieselaar/zaaksysteem.git repo; cd repo; git checkout zs-11922-support-app; mv client ../;

ARG NODE_ENV=production
ENV SITE_BASE=/
ENV SITE_SERVER_NAME=zaaksysteem.nl
ENV ATLASSIAN_HOSTNAME=mintlab.atlassian.com
ENV ATLASSIAN_USERNAME=nonexistent
ENV ATLASSIAN_PASSWORD=nonexistent
ENV ZAAKSYSTEEM_HOSTNAME=nonexistent
ENV ZAAKSYSTEEM_IFACEID1=0
ENV ZAAKSYSTEEM_API_KEY1=nonexistent
ENV ZAAKSYSTEEM_IFACEID2=0
ENV ZAAKSYSTEEM_API_KEY2=nonexistent
ENV PROXY_PATH=/proxy/

COPY conf/http-api-proxy-config.json      /opt/zaaksysteem/conf/http-api-proxy-config.json.tpl
COPY conf/nginx-site.conf                 /opt/zaaksysteem/conf/nginx-site.conf
COPY bin/run.sh                          /opt/zaaksysteem/bin/run.sh
COPY conf/supervisor.conf                 /etc/supervisor/conf.d/support.conf

RUN chmod +x /opt/zaaksysteem/bin/run.sh; \
    cd /opt/zaaksysteem/client; \
    npm install --no-optional; \
    SUPPORT_PROXY_URL=$PROXY_PATH ./node_modules/.bin/webpack --app=support --root=../root --public-path=/assets/ --use-hash=0 --template-path=index.html --base=$SITE_BASE; \
    rm -Rf /opt/zaaksysteem/client; \
    rm /etc/nginx/sites-enabled/default;

EXPOSE 80
CMD ["/opt/zaaksysteem/bin/run.sh"]


