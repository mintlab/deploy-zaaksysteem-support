# Description

This will create a support page from the main zaaksysteem repository

It will create a webserver on port 80 containing a support page

# Setup

```
docker build -t "zaaksysteemnl/support:1.0" .
```

# Run

```
docker run -p 127.0.0.1:80:80 -e "ATLASSIAN_HOSTNAME=mintlab.atlassian.net" -e "ATLASSIAN_USERNAME=username" -e "ATLASSIAN_PASSWORD=p@ssword" -e "ZAAKSYSTEEM_HOSTNAME=blabla.zaaksysteem.nl" -e "ZAAKSYSTEEM_IFACEID1=20" -e "ZAAKSYSTEEM_IFACEID2=29" -e "ZAAKSYSTEEM_API_KEY1=bla87b98a7b97ba98b7a98b7ab" -e "ZAAKSYSTEEM_API_KEY2=bladiebladiebladie" zaaksysteemnl/support:1.0
```

