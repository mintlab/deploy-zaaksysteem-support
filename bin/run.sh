#!/bin/bash


cp /opt/zaaksysteem/conf/nginx-site.conf /etc/nginx/sites-enabled/zaaksysteem.conf
cp /opt/zaaksysteem/conf/http-api-proxy-config.json.tpl /opt/zaaksysteem/conf/http-api-proxy-config.json

sed -i "s/ATLASSIAN_HOSTNAME/${ATLASSIAN_HOSTNAME}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/ATLASSIAN_USERNAME/${ATLASSIAN_USERNAME}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/ATLASSIAN_PASSWORD/${ATLASSIAN_PASSWORD}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/ZAAKSYSTEEM_HOSTNAME/${ATLASSIAN_HOSTNAME}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/ZAAKSYSTEEM_IFACEID1/${ZAAKSYSTEEM_IFACEID1}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/ZAAKSYSTEEM_API_KEY1/${ZAAKSYSTEEM_API_KEY1}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/ZAAKSYSTEEM_IFACEID2/${ZAAKSYSTEEM_IFACEID2}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/ZAAKSYSTEEM_API_KEY2/${ZAAKSYSTEEM_API_KEY2}/" /opt/zaaksysteem/conf/http-api-proxy-config.json
sed -i "s/SITE_SERVER_NAME/${SITE_SERVER_NAME}/" /etc/nginx/sites-enabled/zaaksysteem.conf
sed -i "s|SITE_BASE|${SITE_BASE}|" /etc/nginx/sites-enabled/zaaksysteem.conf
sed -i "s|PROXY_PATH|${PROXY_PATH}|" /etc/nginx/sites-enabled/zaaksysteem.conf

rm /var/log/nginx/{access,error}.log
ln -sf /dev/stdout /var/log/nginx/access.log
ln -sf /dev/stderr /var/log/nginx/error.log

/usr/bin/supervisord

